package com.company;

public class Human extends Species {

    private final int iq;

    public Human(String name, String type, int age, int iq) {
        super(name, type, age);
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    @Override
    public String toString() {
        return super.toString() + " and iq = " + iq;
    }

}
