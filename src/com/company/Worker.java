package com.company;

import java.util.ArrayList;
import java.util.List;

public class Worker {
    List<Species> animals;

    public Worker() {
        animals = new ArrayList<>();
        animals.add(new Species("Monkey", "Mammal", 23));
        animals.add(new Species("Elephant", "Mammal", 35));
        animals.add(new Species("Lizard", "Reptile", 7));
        animals.add(new Species("Hummingbird", "Bird", 13));
        animals.add(new Species("Eagle", "Bird", 22));
        animals.add(new Species("Ladybug", "Insect", 1));
        animals.add(new Species("Tiger", "Mammal", 12));
        animals.add(new Species("Giraffe", "Mammal", 16));
        animals.add(new Species("Chameleon", "Reptile", 6));
        animals.add(new Species("Stag beetle", "Insect", 1));
        animals.add(new Species("Crow", "Bird", 100));
        animals.add(new Species("Mantis", "Insect", 2));
        animals.add(new Human("Ian", "Mammal", 29, 1));
    }

    public void printByType(String type) {
        boolean found = false;
        for (Species animal : animals) {
            if (animal.getType().equalsIgnoreCase(type)) {
                System.out.println(animal);
            found = true;
            }
        }
        if (!found) {
            System.out.println("error");
        }
    }

    public void findByName(String name) {
        boolean found = false;
        for (Species animal : animals) {
            if (name.equals(animal.getName().equalsIgnoreCase(name))) {
                System.out.println(animal);
                found = true;
            }
        }
        if (!found) {
            System.out.println("Error");
        }
    }

    public void findLessAge(int age) {
        for (Species animal : animals) {
            if (animal.getAge() <= age) {
                System.out.println(animal);
            }
        }
    }
    public void findTypeAndAge (String type, int age) {
        for (Species animal : animals) {
            if (animal.getType().equals(type) && animal.getAge() >= age) {
                System.out.println(animal);
            }
        }
    }

}