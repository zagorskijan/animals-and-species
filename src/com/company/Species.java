package com.company;

public class Species {

    private final String name;

    private final String type;

    private final int age;

    public Species(String name, String type, int age) {
        this.name = name;
        this.type = type;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return String.format("%s %s: %s", name, type, age);
    }

}
